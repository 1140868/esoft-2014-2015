#Enunciado da Iteração 2

## Respostas a perguntas em aberto
1. Um novo utilizador é válido e único se:

    + O seu endereço de e-mail é válido e não é usado por mais nenhum utilizador, e
    + O seu username não é usado por mais nenhum utilizador.

2. Portanto, a forma de identificar um utilizador é através do seu endereço de e-mail ou username.
3. Um Organizador, Revisor e Autor Correspondente é identificado pelo seu email ou username de Utilizador.
4. Um Autor é identificado pelo seu endereço de e-mail que deve ser único no artigo mas não tem que corresponder a um Utilizador.
5. O autor correspondente é um autor do artigo e é obrigatoriamente um utilizador.

## Novos Requisitos
Um evento pode conter várias sessões temáticas definidas pelos organizadores do evento. Cada sessão temática corresponde a um código único (no evento), uma descrição e à identificação das pessoas proponentes da mesma que são simultaneamente os responsáveis pela gestão das tarefas relativas a essa sessão. Uma dessas tarefas é a especificação da Comissão de Programa da sessão. Todos os proponentes são utilizadores do sistema.

Durante a submissão de artigos é necessário indicar se o artigo está a ser submetido ao evento principal ou a uma das suas sessões. Neste último caso, deve ser indicada qual é a sessão.

Findo o período de submissão de artigos, os organizadores/proponentes despoletam o processo de distribuição de revisões que consiste na atribuição a cada artigo submetido de um ou mais revisores responsáveis por analisarem/avaliarem individualmente o artigo. Findo o processo de análise/avaliação, os revisores devem indicar no sistema para cada artigo se o mesmo deve ser aceite ou recusado juntamente com um breve texto justificativo.

Como forma de automatizar o processo de distribuição de revisões são disponibilizados vários mecanismos. Estes mecanismos são baseados em diferentes critérios (e.g. número de revisores pretendidos por artigo, distribuição de carga equitativa pelos revisores, afinidade entre tópicos dos artigos e tópicos em que o revisor é perito). O resultado da distribuição pode ser alterado através da escolha de diferentes mecanismos até que um seja confirmado como definitivo pelo organizador/proponente.

O processo de registo de um utilizador deve ser muito rápido (e.g. máximo 2 minutos).
Perto das datas limites de submissão de artigos a um evento/sessão prevê-se uma sobrecarga maior do sistema.

Considerando os diversos intervenientes no sistema e a diversidade das suas características é fundamental que a interação entre os utilizadores e o sistema seja simples, intuitiva e completamente adaptada à ação em causa. 

O software deve ser desenvolvido de forma iterativa e incremental, bem como adotar boas práticas de design (e.g. padrões GRASP) e normas de codificação.