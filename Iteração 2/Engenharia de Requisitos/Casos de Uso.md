# Diagrama de Casos de Uso #
*TBD*

# Casos de Uso
+ [UC1: Registar Utilizador](Casos De Uso/UC1 - Registar Utilizador)
+ [UC2: Criar Evento Científico](Casos De Uso/UC2 - Criar Evento Científico)
+ [UC3: Criar Comissão de Programa](Casos De Uso/UC3 - Criar Comissão de Programa)
+ [UC4: Submeter Artigo Científico](Casos De Uso/UC4 - Submeter Artigo Científico)
+ [UC5: Rever Artigo Científico](Casos De Uso/UC5 - Rever Artigo Científico)
+ [UC6: Definir Sessão Temática](Casos De Uso/UC6 - Definir Sessão Temática)
+ [UC7: Criar CP de Sessão Temática](Casos De Uso/UC7 - Criar CP de Sessão Temática)
+ [UC8: Distribuir revisões de artigos](Casos De Uso/UC8 - Distribuir revisões de artigos)