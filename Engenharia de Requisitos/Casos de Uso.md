﻿# Diagrama de Casos de Uso #
![Use Case Diagram1.jpg](https://bitbucket.org/repo/yjGXBn/images/222098793-Use%20Case%20Diagram1.jpg)

# Casos de Uso
+ [UC1: Registar utilizador](Casos de Uso/UC1 - Registar Utilizador)
+ [UC2: Criar Evento Científico](Casos de Uso/UC2 - Criar Evento Científico)
+ [UC3: Criar Comissão de Programa](Casos de Uso/UC3 - Criar Comissão de Programa)
+ [UC4: Submeter Artigo Científico](Casos de Uso/UC4 - Submeter Artigo Científico)