##UC2 - Criar Evento Cientifico##

##Formato Breve:##
O Utilizador introduz no sistema os dados relativos ao artigo científico (titulo, descrição, data de inicio, data de fim, local, organizadores). O sistema valida os dados e pede confirmação. O utilizador confirma os dados inseridos. O sistema regista os dados e informa o utilizador do sucesso da operação.

##SSD do formato Breve:##
![Sequence Diagram1.jpg](https://bitbucket.org/repo/yjGXBn/images/2848562922-Sequence%20Diagram1.jpg)

##Formato Completo:##
###Ator principal:###
+ Administrador;

###Partes interessadas e seus interesses:###
+ Administrador – criação de eventos científicos fácil e sem problemas;
+ Empresa – ter uma aplicação que lhes permite ter uma boa realização de eventos científicos.

###Pré-condições:###
+ O utilizador tem que estar autenticado como administrador.

###Pós-condições:###
+ O registo do evento fica armazenado no sistema.

###Cenário de sucesso principal (ou fluxo básico):###
1.    O administrador inicia no sistema o registo de um evento;
2.    O sistema solicita os dados do evento (titulo, descrição, data de inicio, data de fim, local da realização)
3.    O administrador introduz os dados solicitados;
4.    O sistema valida e apresenta os dados introduzidos para confirmação;
5.    O administrador confirma a informação;
6.    O sistema solicita a introdução dos organizador do evento;
7.    O administrador introduz a identificação de um organizador;
8.    O sistema valida e guarda a informação do organizador;
9.    Os passos 7 e 8 repetem-se até que todos os organizadores do evento estejam inseridos;
10.  O Sistema regista o evento e informa o administrador do sucesso da operação.


###Extensões (ou fluxo alternativos):###
*a. O administrador solicita o cancelamento da criação de evento. 

+ O caso de uso termina.

4a. Dados mínimos obrigatórios em falta.

1.	O sistema informa quais os dados em falta.
2.	O sistema permite a introdução dos dados em falta (passo 3).

4b. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.

1.	O sistema alerta o utilizador para o facto.
2.	O sistema permite a sua alteração (passo 3).

8a. Dados mínimos obrigatórios em falta.

1.	O sistema informa quais os dados em falta.
2.	O sistema permite a introdução dos dados em falta (passo 7).

8b. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.

1.	O sistema alerta o utilizador para o facto.
2.	O sistema permite a sua alteração (passo 7).

###Requisitos especiais:###
-

###Tecnologia e Lista de Variações dos Dados:###
-

###Frequência de Ocorrência:###
-

###Questões em Aberto:###
+ O local limita-se apenas a uma cidade, ou pode incluir também um país ou instituição em simultâneo;
+ Deve ser considerado algum fuso horário específico relativo ao local?
+ Quais são os dados obrigatórios para a criação do evento?
+ Quais os dados exigidos para os organizadores do evento?
+ O que fazer quando um dos organizadores não se encontrar registado no sistema?
+ Pode ocorrer eventos em simultâneo?
+ Quais os dados que em conjunto permitem detetar a duplicação de evento?
+ Alguém deve ser notificado da criação do evento, por exemplo, utilizadores registados?
+ Qual a frequência de ocorrência deste caso de uso?