# UC3: Criar comissão de programa #
##           Formato breve ##
O organizador introduz no sistema os dados relativos à Comissão de Programa (dados dos revisores). O sistema valida os dados e pede confirmação. O utilizador confirma os dados inseridos. O sistema regista os dados e informa o utilizador do sucesso da operação.

##           SSD de formato breve ##
![Sequence Diagram1.jpg](https://bitbucket.org/repo/yjGXBn/images/1412611930-Sequence%20Diagram1.jpg)

## Formato completo ##

### Actor principal ###
Organizador

### Partes interessadas e seus interesses ###
 + Empresa: ter informações sobre os membros da Comissão de Programa
 + Organizador: ter uma plataforma para inserção dos membros da Comissão de Programa

### Pré-condições ###
 + O organizador tem que estar autenticado

### Pós-condições ###
 + A Comissão de Programa associada ao evento fica armazenada no sistema

### Cenário de sucesso principal (ou fluxo básico) ###
1.	O organizador inicia no sistema o registo da comissão de programa;
2.	O sistema apresenta os eventos futuros para os quais o utilizador é organizador.
3.	O organizador selecciona o evento.
4.	O sistema solicita um revisor (identificação);
5.	O organizador introduz os dados solicitados;
6.	O sistema valida e associa o revisor à CP;
7.	Os passos 4 a 6 repetem-se até que todos os revisores estejam inseridos;
8.	O sistema apresenta os dados introduzidos para confirmação;
9.	O organizador confirma a informação apresentada;
10.	O sistema guarda a CP do evento informa o organizador do sucesso da operação.

###Extensões (ou fluxos alternativos)###
*a. O organizador solicita o cancelamento.

 + O caso de uso termina.

6a. Dados mínimos obrigatórios em falta.

1. O sistema informa quais os dados em falta.
2. O sistema permite a introdução dos dados em falta (passo 5)

6b. O sistema detecta que os dados introduzidos (ou algum subconjunto dos dados) são inválidos.

1. O sistema alerta o utilizador para o facto.
2. O sistema permite a sua alteração (passo 5).

6c. O sistema detecta que o revisor já consta na  CP.

1. O sistema alerta o utilizador para o facto.
2. O sistema permite a sua alteração (passo 4).

###Requisitos especiais###
-

###Tecnologia e Lista de Variações dos Dados###
-

###Frequência de Ocorrência###
-

###Questões em aberto###
 + Será possível editar a Comissão de Programa posteriormente à sua criação?
 + É necessário fazer uma validação global antes da atribuição da CP ao evento ?
 + Quais as informações necessárias relativamente aos membros da CP ?
