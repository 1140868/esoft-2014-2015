﻿##UC4: Submeter Artigo Científico##

##Formato breve##
O utilizador faz *upload* do artigo e introduz os respetivos dados (autores, título, resumo, autor correspondente). O sistema valida os dados e pede confirmação. O utilizador confirma os dados inseridos. O sistema armazena o artigo e respetivos dados e informa o utilizador do sucesso da operação.

##SSD de formato breve ##
![Sequence Diagram1.jpg](https://bitbucket.org/repo/yjGXBn/images/288221757-Sequence%20Diagram1.jpg)

## Formato completo ##

###Ator principal ###
+ Utilizador registado

###Partes interessadas e seus interesses###
 + Empresa: obter artigos para os seus eventos.
 + Utilizador: submeter artigos para um dado evento.

###Pré-condições###
+ Haver um evento futuro.

###Pós-condições###
+ O artigo fica armazenado no sistema para posterior revisão.

###Cenário de sucesso principal (ou fluxo básico)###
1.	O utilizador selecciona qual o evento futuro para o qual pretende submeter o artigo;
2.	O sistema pede ao utilizador que este faça *upload* do ficheiro (artigo);
3.	O utilizador faz *upload* do ficheiro;
4.	O sistema solicita os dados do artigo (autores, título, resumo, autor correspondente);
5.	O utilizador introduz os dados solicitados;
6.     O sistema solicita a introdução dos autores do artigo;
7.     O utilizador introduz a identificação de um autor;
8.     O sistema valida e guarda os dados do autor;
9.     Os passos 6 a 8 repetem-se até todos os autores estejam inseridos;
10.	O sistema valida e solicita ao utilizador que confirme os dados inseridos;
11.	O utilizador valida os dados;
12.	O sistema armazena o ficheiro (artigo) e respetivos dados e informa o utilizador do sucesso da operação.


###Extensões (ou fluxos alternativos)###
*a. O utilizador solicita o cancelamento.

+ O caso de uso termina.

3ª. O utilizador tenta fazer *upload* de um ficheiro num formato não suportado.

1.	O sistema alerta o utilizador para o facto;
2.	O sistema permite uma nova submissão (passo 2).

5a. Dados mínimos obrigatórios em falta.

1.	O sistema informa quais os dados em falta.
2.	O sistema permite a introdução dos dados em falta (passo 4)

7a. Dados mínimos obrigatórios em falta.

1.	O sistema informa quais os dados em falta.
2.	O sistema permite a introdução dos dados em falta (passo 6)


###Requisitos especiais###
-

###Tecnologia e Lista de Variações dos Dados###
 + Artigo deverá estar em formato PDF.

###Frequência de Ocorrência###
-

###Questões em aberto###
+ Qual a data limite para a submissão de um artigo para um dado evento?
+ Haverá um limite relativamente ao tamanho do artigo a submeter?
+ É necessário que todos os autores do artigo estejam registados?
+ Será possível submeter artigo num formato que não PDF?
+ Quantos artigos pode um utilizador submeter para um dado evento?
+ Haverá um limite de caracteres para o resumo do artigo?