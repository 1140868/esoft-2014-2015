﻿#Glossário#
 + Administrador - pessoa responsável pela criação dos eventos.

 + Artigo (Científico) - trabalho com resultados sucintos de uma pesquisa realizada de acordo com um método científico por um ou mais autores.

 + Autor - pessoa que escreveu ou produziu, parcial ou totalmente, o artigo.

 + Comissão de Programa - equipa responsável pela revisão dos artigos de um dado evento (revisores).
 
 + Evento (Científico) - acontecimento planeado cujo o objetivo é divulgar estudos científicos (artigos científicos) relativos a um tema específico.

 + Organizador - pessoa responsável por definir a Comissão de Programa.

 + Revisor - pessoa responsável pela avaliação dos artigos.

 + Upload - submeter/enviar ficheiros ou dados de um computador local para a internet ou um servidor.
