﻿# Atribuição de Responsabilidades #

+ [UC1: Registar utilizador](Tabela Responsabilidades/UC1 - Registar Utilizador)
+ [UC2: Criar Evento Científico](Tabela Responsabilidades/UC2 - Criar Evento Científico)
+ [UC3: Criar Comissão de Programa](Tabela Responsabilidades/UC3 - Criar Comissão de Programa)
+ [UC4: Submeter Artigo Científico](Tabela Responsabilidades/UC4 - Submeter Artigo Científico