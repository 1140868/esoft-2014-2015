#	Análise OO
O processo de construção do modelo de domínio é baseado nos casos de uso, em especial os substantivos utilizados, e na descrição do enunciado.
##	Racional para identificação de classes de domínio
Para a identificação de classes de domínio usa-se a lista de categorias das aulas TP (sugeridas no livro). Como resultado temos a seguinte tabela de conceitos (ou classes, mas não de software) por categoria: 

Categoria: Conceitos

+	Transações  (do negócio): Evento, Submissão (de artigo), Revisão (de submissão)
+	Linhas de transações : Revisão (de submissão por revisor)
+	Produtos ou serviços relacionados com transações: Evento, Artigo, Revisão
+	Registos (de transações): 
+	Papéis das pessoas: Administrador, Revisor, Autor, Organizador, Utilizador
+	Lugares: Local de evento
+	Eventos: Evento científico, Submissão de artigo, Revisão de artigo
+	Objetos físicos: 
+	Especificações e descrições: 
+	Catálogos: 
+	Conjuntos (containers?): Comissão de Programa, Lista de Eventos Científicos
+	Elementos de conjuntos: Revisor, Organizador, Evento Científico, Administrador, Submissão, Utilizador
+	(Outras) Organizações: Instituição de afiliação, Empresa
+	Outros sistemas (externos): 
+	Registos (financeiros), de trabalho, contractos, documentos legais: 
+	Instrumentos financeiros: 
+	Documentos referidos/para executar as tarefas: Documento PDF (do artigo)
##	Racional sobre identificação de associações entre classes
Uma associação é uma relação entre instâncias de objectos que indica uma conexão relevante e que vale a pena recordar, ou é derivável da Lista de Associações Comuns: 
+	A é fisicamente (ou logicamente) parte de B
+	A está fisicamente (ou logicamente) contido em B
+	A é uma descrição de B
+	A é conhecido/capturado/registado por B
+	A usa ou gere B
+	A está relacionado com uma transacção de B
+	etc.
###	Evento Científico
+	Criado por - Administrador
+	Tem - Organizadores
+	Tem - Comissão de Programa
+	Registado - Empresa
+	Ocorre em - Local
+	Tem – Submissões
###	Submissão
+	De – Artigo
+	No âmbito de – Evento científico
+	Feita por – Utilizador
###	Revisão
+	Realizada por – Revisor
+	Sobre – Artigo
###	Artigo
+	Relacionado com – Submissão
+	Tem – Autor
+	Tem – Revisão
+	Tem Correspondente – Autor Correspondente
###	Administrador
+	Cria – Evento Científico
+	Registado como – Utilizador
###	Revisor
+	Efetua – Revisão
+	Registado como – Utilizador
+	Registado em – Empresa
###	Autor
+ de – Artigo
+ Registado como – Utilizador
###	Utilizador
+ faz – Registo de utilizador
+ faz – Submissão
###	Organizador
+ especifica – Comissão de Programa
+ registado como – Utilizador
+ de – Evento
###	Local de evento
+ onde ocorre – Evento Científico
###	Comissão de Programa
+ registado em cada – Evento Científico
###	Instituição de afiliação
+ referente a – Autor
+ referente a – Utilizador
###	Documento PDF
+ materialização de - Artigo

##	Diagrama de MD
![MD it1.png](https://bitbucket.org/repo/aA9RGB/images/913954156-MD%20it1.png)
