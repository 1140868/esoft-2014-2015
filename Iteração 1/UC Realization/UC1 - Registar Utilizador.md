#	UC1: Registar Utilizador
##	Racional
1.	O ator inicia no sistema o seu registo.
    1.	Questão: Que classe interage com o ator?
        +	Resposta: A classe RegistarUtilizadorUI.
        +	Justificação: Pure Fabrication, pois não se justifica atribuir esta responsabilidade a nenhuma classe existe no Modelo de Domínio.
    1.	Questão: Quem coordena o UC?
        +	Resposta: RegistarUtilizada classe orController.
        +	Justificação: Controller.
    1.	Questão: Que classe instancia Utilizador?
        +	Resposta: Empresa
        +	Justificação: Creator (Regra 1)
2.	O sistema solicita os dados do utilizador (credenciais de acesso, nome, e-mail, instituição de afiliação).
    2.	Questão: Que classe solicita os dados?
        +	Resposta: RegistarUtilizadorUI.
        +	cf. 1.1
3.	O utilizador  introduz os dados solicitados.
    3.	Questão: Onde ser guardam os dados do utilizador?
        +	Resposta: Utilizador
        +	Justificação: Information Expert
4.	O sistema valida e regista os dados do utilizador.
    4.	Questão: Que classe valida os dados de cada Utilizador (validação local)?
        +	Resposta: Utilizador
        +	Justificação: IE
    4.	Questão: Que classe valida a lista de Utilizadores (validação global)?
        +	Resposta: Empresa
        +	Justificação: IE
    4.	Questão: Que classe guarda o Utilizador?
        +	Resposta: Empresa
        +	Justificação: IE. No MD a Empresa contém/agrega Utilizador(es)
5.	O sistema notifica o utilizador do sucesso da operação e apresenta os dados inseridos no sistema.
    5.	Questão: Que classe notifica?
        +	Resposta: RegistarUtilizadorUI
        +	Justificação: cf. 1.1
    5.	Questão: Que classe tem os dados para serem apresentados?
        +	Resposta: Utilizador
        +	Justificação: IE
##	Diagrama de Sequência
![UC1 realization.png](https://bitbucket.org/repo/aA9RGB/images/704236257-UC1%20realization.png)
##	Diagrama de Classes
![UC1 DC.jpg](https://bitbucket.org/repo/aA9RGB/images/328775356-UC1%20DC.jpg)
 
