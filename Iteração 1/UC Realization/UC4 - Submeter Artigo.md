#	UC4: Submeter Artigo
##	Racional
1.	O organizador inicia no sistema a Submissão de Artigo.
    1.	Questão: Que classe interage com o ator?
        +	Resposta: SubmeterArtigoUI
        +	Justificação: [cf. UC1 realization](UC1 - Registar Utilizador)
    1.	Questão: Que classe coordena o UC?
        +	Resposta: SubmeterArtigoController
        +	Justificação: [cf. UC1 realization](UC1 - Registar Utilizador)
    1.	Questão: Que classe instancia Submissão?
        +	Resposta: Evento
        +	Justificação: Creator (Regra 1)
1.	O sistema mostra os dados dos eventos em que o utilizador é organizador
    1.	Questão: Que classe mostra os eventos?
        +	[cf. UC1 realization](UC1 - Registar Utilizador)
    1.	Questão: Que classe tem os eventos do utilizador/organizador?
        +	Resposta: Empresa
        +	Justificação IE (Empresa regista todos os Eventos)
2.	O organizador seleciona o evento científico pretendido.
    2.	Questão: Que classe armazena esta informação?
        +	Resposta: SubmeterArtigoController
        +	Justificação: Controller coordena o UC para cada evento
3.	O sistema solicita o título e o resumo do artigo bem como a lista de autores na qual consta inicialmente apenas o utilizador que está a submeter o artigo.
       +	[cf. UC1 realization](UC1 - Registar Utilizador)
4.	O utilizador  introduz o título e o resumo do artigo.
    4.	Questão Que classe guarda estes dados?
        +	Resposta: Artigo
        +	Justificação: IE
    4.	Questão: Que classe instancia o objeto Artigo?
        +	Resposta: Submissão
        +	Justificação: Creator (R3 ou R4)
5.	O utilizador introduz os dados de um autor do artigo.
    5.	Questão: Quem guarda estes dados?
        +	Resposta: Autor
        +	Justificação: IE
    5.	Questão: Que classe instancia Autor?
        +	Resposta: Artigo
        +	Justificação: Creator (R1)
6.	O sistema valida e solicita confirmação dos dados do autor.
    6.	Questão: Que classe valida os dados de cada Autor (validação local)?
        +	Resposta: Autor
        +	Justificação: IE
    6.	Questão: Que classe valida a lista de Autores (validação global)?
        +	Resposta: Artigo
        +	Justificação: IE
    6.	Questão: Que classe guarda o Autor?
        +	Resposta: Artigo
        +	Justificação: IE
    6.	Questão: Que classe associa o objeto Utilizador ao objeto Autor?
        +	Resposta: Artigo
        +	Justificação: IE (Artigo conhece Autor e Utilizador, sendo este passado por parâmetro ao Artigo)
7.	O utilizador confirma os dados do autor.
       + [cf. UC1 realization](UC1 - Registar Utilizador)
8.	Os passos 6 a 8 são repetido até que na lista constem todos os autores do artigo.
9.	O sistema questiona qual dos autores introduzidos é o autor correspondente.
    9.	Questão: Que classe conhece os autores possíveis de serem autor correspondente?
        +	Resposta: Artigo
        +	Justificação: IE
10.	O utilizador indica quem é o autor correspondente.
    10.	Questão: Que classe valida/guarda o Autor correspondente?
        +	Reposta: Artigo
        +	Justificação: IE
11.	O sistema solicita o ficheiro PDF correspondente ao conteúdo do artigo.
       + [cf. UC1 realization](UC1 - Registar Utilizador)
12.	O utilizador indica o respetivo ficheiro.
    12.	Questão: Que classe quarda o ficheiro?
        +	Resposta: Artigo
        +	Justificação: IE
13.	O sistema valida e apresenta os dados introduzidos para confirmação.
    13.	Questão: Que classe valida os dados do artigo (validação local)?
        +	Resposta: Artigo
        +	Justificação: IE
    13.	Questão: Que classe valida os dados de cada submissão (validação local/global)?
        +	Resposta: Submissão
        +	Justificação: IE
    13.	Questão: Que classe valida a lista de submissões (validação global)?
        +	Resposta: Evento
        +	Justificação: IE
14.	O utilizador confirma a informação apresentada.
       + [cf. UC1 realization](UC1 - Registar Utilizador)
15.	O sistema informa o utilizador do sucesso da operação.
    15.	Questão: Que classe guarda a Submissão?
        +	Resposta: Evento
        +	Justificação: IE

##	Diagrama de Sequência
![UC4 DS.png](https://bitbucket.org/repo/aA9RGB/images/1749337031-UC4%20DS.png)

###	Diagrama de Classes
![UC4 DC.png](https://bitbucket.org/repo/aA9RGB/images/1361592238-UC4%20DC.png)