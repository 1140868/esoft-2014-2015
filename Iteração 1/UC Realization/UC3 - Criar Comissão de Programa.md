#	UC3: Criar Comissão de Programa
##	Racional
1.	O organizador inicia no sistema o registo da CP.
    1.	Questão: Que classe interage com o ator?
        +	Resposta: CriarCPUI
        +	Justificação: [cf. UC1 realization](UC1 - Registar Utilizador)
    1.	Questão: Que classe coordena o UC?
        +	Resposta: CriarCPController
        +	Justificação: [cf. UC1 realization](UC1 - Registar Utilizador)
    1.	Questão: Que classe instancia CP?
        +	Resposta: Evento
        +	Justificação: Creator (Regra 1)
2.	O sistema mostra os dados dos eventos em que o utilizador é organizador
    2.	Questão: Que classe mostra os eventos?
        +	[cf. UC1 realization](UC1 - Registar Utilizador)
    2.	Questão: Que classe tem os eventos do utilizador/organizador?
        +	Resposta: Empresa
        +	Justificação IE (Empresa regista todos os Eventos)
3.	O organizador seleciona o evento científico pretendido.
    3.	Questão: Que classe armazena esta informação?
        +	Resposta: CriarCPController
        +	Justificação: Controller coordena o UC para cada evento
4.	O sistema solicita a identificação dum utilizador para membro da CP.
       * [cf. UC1 realization](UC1 - Registar Utilizador)
5.	O organizador introduz o identificador do novo membro.
    5.	Questão: Como obter através do identificador o organizador respetivo?
        +	Resposta: Empresa
        +	IE: Empresa conhece todos os utilizadores e por conseguinte todos os seus identificadores. O resultado é um  objeto Utilizador.
    5.	Questão: Que classe instancia o objeto Revisor?
        +	Resposta: CP
        +	Justificação: Creator (R1)
    5.	Questão: Que classe associa o objeto Utilizador ao objeto Revisor?
        +	Resposta: CP
        +	Justificação: IE (CP conhece Revisor e Utilizador, sendo este passado por parâmetro à CP)
6.	O sistema valida e solicita confirmação.
    6.	Questão: Que classe valida os dados de cada Revisor (validação local)?
        +	Resposta: Revisor
        +	Justificação: IE
    6.	Questão: Que classe valida a lista de Revisores (validação global)?
        +	Resposta: CP
        +	Justificação: IE
7.	O organizador confirma os dados.
       * [cf. UC1 realization](UC1 - Registar Utilizador)
8.	O sistema regista o novo membro.
    8.	Questão: Que classe guarda o Revisor?
        +	Resposta: CP
        +	Justificação: IE. No MD a CP contém/agrega Revisor(es)
9.	Os passos 4-8 são repetidos até a CP estar completa.
10.	O sistema atribui a CP ao evento e avisa o organizador que a operação foi realizada com sucesso.
    10.	Questão: Que classe faz a atribuição?
        +	Resposta: Evento
        +	Justificação: IE (no MD o Evento tem CP)
    10.	Questão: Que classe informa o Administrador do sucesso da operação?
        +	[cf. UC1 realization](UC1 - Registar Utilizador)
##	Diagrama de Sequência
![UC3 DS.png](https://bitbucket.org/repo/aA9RGB/images/275122445-UC3%20DS.png)

##	Diagrama de Classes
![UC3 DC.png](https://bitbucket.org/repo/aA9RGB/images/4100643755-UC3%20DC.png)