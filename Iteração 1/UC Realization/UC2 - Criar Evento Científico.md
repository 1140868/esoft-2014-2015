#	UC2: Criar Evento Científico
##	Racional
1.	O administrador inicia no sistema o registo de um evento.
    1.	Questão: Quem interage com o ator?
        +	Resposta: CriarEventoUI
        +	Justificação: [cf. UC1 realization](UC1 - Registar Utilizador)
    1.	Questão: Quem coorderna o UC?
        +	Resposta: CriarEventoController
        +	Justificação: [cf. UC1 realization](UC1 - Registar Utilizador)
    1.	Questão: Que classe instancia Evento?
        +	Resposta: Empresa
        +	Justificação: Creator (Regra 1)
2.	O sistema solicita os dados do evento (título, descrição, data de início, data de fim, o local de realização).
    2.	Questão: Que classe solicita os dados?
        +	Resposta: CriarEventoUI
        +	Justificação: [cf. UC1 realization](UC1 - Registar Utilizador)
3.	O administrador  introduz os dados solicitados.
    3.	Questão: Onde ser guardam os dados do Evento?
        +	Resposta: Evento
        +	Justificação: Information Expert
    3.	Questão: Que classe instancia Local?
        +	Resposta: Evento
        +	Justificação: Creator (R3 ou R4)
4.	O sistema solicita a introdução dos organizadores do evento.
    4.	Questão: Que classe solicita a introdução?
        +	[cf. UC1 realization](UC1 - Registar Utilizador)
5.	O administrador introduz a identificação de um organizador.
    5.	Questão: Como obter através do identificador o organizador respetivo?
        +	Resposta: Empresa
        +	IE: Empresa conhece todos os utilizadores e por conseguinte todos os seus identificadores. O resultado é um  objeto Utilizador.
    5.	Questão: Que classe instancia o objeto Organizador?
        +	Resposta: Evento
        +	Justificação: Creator (R1)
    5.	Questão: Que classe associa o objeto Utilizador ao objeto Organizador?
        +	Resposta: Evento
        +	Justificação: IE (Evento conhece/tem o objeto Organizador)
6.	O sistema valida e guarda os dados do Organizador.
    6.	Questão: Que classe valida os dados de cada Organizador (validação local)?
        +	Resposta: Organizador
        +	Justificação: IE
    6.	Questão: Que classe valida a lista de Organizadores (validação global)?
        +	Resposta: Empresa
        +	Justificação: IE
    6.	Questão: Que classe guarda o organizador?
        +	Resposta: Empresa
        +	Justificação: IE. No MD a Empresa contém/agrega Organizador(es)
7.	Os passos 5 a 6 repetem-se até que todos os organizadores estejam inseridos.
8.	O sistema valida e apresenta todos os dados introduzidos para confirmação.
    8.	Questão: Que classe valida os dados de cada Evento (validação local)?
        +	Resposta: Evento
        +	Justificação: IE
    8.	Questão: Que classe valida a lista de Eventos (validação global)?
        +	Resposta: Empresa
        +	Justificação: IE
    8.	Questão: Que classe apresenta os dados?
        +	[cf. UC1 realization](UC1 - Registar Utilizador)
9.	O administrador confirma informação apresentada
        +	n/a
10.	O sistema regista o evento e informa o administrador do sucesso da operação.
    10.	Questão: Que classe regista o Evento?
        +	Resposta: Empresa
        +	Justificação: IE (no MD a Empresa regista Evento)
    10.	Questão: Que classe informa o Administrador do sucesso da operação?
        +	[cf. UC1 realization](UC1 - Registar Utilizador)
## Sistematização
Do racional resulta que as classes conceptuais promovidas a classes de software são:

+ Empresa
+ Evento
+ Organizador
+ Utilizador
+ Local

Outras classes de software identificadas:

+ CriarEventoUI
+ CriarEventoController

###	Diagrama de Sequência
![UC2 DS.png](https://bitbucket.org/repo/aA9RGB/images/3834694656-UC2%20DS.png)

###	Diagrama de Classes (Incrementado ao anterior)
![UC2 DC.png](https://bitbucket.org/repo/aA9RGB/images/3893806458-UC2%20DC.png)
