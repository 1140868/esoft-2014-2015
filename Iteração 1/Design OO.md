# Realização de casos de uso

* [UC1: Registar Utilizador](UC Realization/UC1 - Registar Utilizador)
* [UC2: Criar Evento Científico](UC Realization/UC2 - Criar Evento Científico)
* [UC3: Criar Comissão de Programa](UC Realization/UC3 - Criar Comissão de Programa)
* [UC4: Submeter Artigo](UC Realization/UC4 - Submeter Artigo)