# Diagrama de Casos de Uso #
![Use Case Diagram.jpg](https://bitbucket.org/repo/aA9RGB/images/2023931920-Use%20Case%20Diagram.jpg)

# Casos de Uso
+ [UC1: Registar Utilizador](Casos De Uso/UC1 - Registar Utilizador)
+ [UC2: Criar Evento Científico](Casos De Uso/UC2 - Criar Evento Científico)
+ [UC3: Criar Comissão de Programa](Casos De Uso/UC3 - Criar Comissão de Programa)
+ [UC4: Submeter Artigo Científico](Casos De Uso/UC4 - Submeter Artigo Científico)
+ [UC5: Rever Artigo Científico](Casos De Uso/UC5 - Rever Artigo Científico)