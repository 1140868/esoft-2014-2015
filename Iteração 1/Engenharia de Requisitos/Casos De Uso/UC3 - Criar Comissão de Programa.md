#	UC3: Criar Comissão de Programa
##	Formato breve
O organizador inicia o processo de inserção da Comissão de Programa. O sistema apresenta todos os eventos para os quais é um dos organizadores. O organizador indica qual o evento a considerar. O sistema solicita os dados de cada membro da Comissão de Programa. O organizador insere os dados. O sistema apresenta os dados e o organizador confirma os mesmos. O sistema atualiza a Comissão de Programa do evento.

## SSD de formato breve
![ssd-uc3.jpg](https://bitbucket.org/repo/aA9RGB/images/1730703423-ssd-uc3.jpg)
 
##	Formato completo
###	Ator principal
Organizador
###	Partes interessadas e seus interesses
+	Organizador: rapidez no registo da Comissão de Programa (CP) e sem erros.
+	CP: registo sem erros dos seus dados.
+	Empresa: funcionamento correto da aplicação.
###	Pré-condições
+	O Utilizador está identificado e autenticado no sistema.
###	Pós-condições
+	A informação da Comissão de Programa de um evento é registada no sistema.
+	Implica a classificação dos membros da CP como “Revisores”.
###	Cenário de sucesso principal (ou fluxo básico)
1.	O organizador inicia no sistema o registo da CP.
2.	O sistema mostra a lista de eventos em que o utilizador é organizador.
3.	O organizador seleciona o evento científico pretendido.
4.	O sistema solicita a identificação dum utilizador para membro da CP.
5.	O organizador introduz o identificador do novo membro.
6.	O sistema valida e solicita confirmação.
7.	O organizador confirma os dados.
8.	O sistema regista o novo membro.
9.	Os passos 4-8 são repetidos até a CP estar completa.
10.	O sistema atribui a CP ao evento e avisa o organizador que a operação foi realizada com sucesso.
###	Extensões (ou fluxos alternativos)
*a. A qualquer momento o organizador cancela o processo. 
+ O caso de uso termina.

2a. Não há eventos nas condições especificadas.
+ O sistema avisa o organizador e o caso de uso termina.

6a. A identificação introduzida não corresponde a nenhum utilizador do sistema.

1.	O sistema alerta para o facto.
2.	O sistema permite a alteração da identificação introduzida (Passo 4).	

6b. O sistema deteta que o membro indicado já está incluído na CP.

   1. O sistema alerta para o facto.
   2. O sistema permite a alteração da identificação introduzida (Passo 4).	
###	Requisitos especiais
-
###	Tecnologia e Lista de Variações dos Dados
-
###	Frequência de Ocorrência
-
###	Questões em aberto
+	Alguma documentação é exigida para a inserção de um membro da Comissão de Programa? Se alguma é exigida, a mesma deve ficar associada ao seu registo?
+	Quais os dados exigidos para os membros da CP? E desses dados, quais devem ser únicos?
+	Que dados (para além do utilizador) permitem evitar a inserção duplicada de um membro da CP?
+	Alguma pessoa deve ser notificada da inserção de novos membros da CP de um evento no sistema como, por exemplo, o seu presidente? Todos os organizadores devem receber também uma notificação?
