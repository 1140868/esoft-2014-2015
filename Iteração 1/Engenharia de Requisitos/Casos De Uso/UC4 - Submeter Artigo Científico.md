#	UC4: Submeter Artigo Científico
##	Formato breve
O utilizador insere no sistema os dados relativos a um artigo científico (e.g. evento, lista de autores, título, resumo, autor correspondente) e indica qual o ficheiro PDF que constitui o artigo. O sistema valida e apresenta os dados introduzidos. O utilizador confirma e o sistema guarda os dados informando o utilizador do sucesso da operação.

## SSD de formato breve
![UC4.png](https://bitbucket.org/repo/aA9RGB/images/1855508349-UC4.png)
##	Formato completo
###	Ator principal
+	Utilizador
###	Partes interessadas e seus interesses
+	Utilizador/autor submissor: facilidade na submissão
+	Organizadores do evento: garantir que os utilizadores conseguem submeter artigos corretamente.
+	CP: que os artigos submetidos disponham das informações necessárias para fazer o seu trabalho.
###	Pré-condições
+	utilizador está identificado e autenticado no sistema.
###	Pós-condições
+	A informação do artigo submetido fica registada no sistema e disponível para ser explorada.
+	O utilizador que submete o artigo é classificado como “Autor”.
###	Cenário de sucesso principal (ou fluxo básico)
1.	O utilizador inicia no sistema o processo de submissão de um artigo.
2.	O sistema mostra uma lista dos eventos científicos aos quais é possível submeter artigos.
3.	O utilizador seleciona um evento.
4.	O sistema solicita o título e o resumo do artigo bem como a lista de autores na qual consta inicialmente apenas o utilizador que está a submeter o artigo. 
5.	O utilizador  introduz o título e o resumo do artigo.
6.	O utilizador introduz os dados de um autor do artigo.
7.	O sistema valida e solicita confirmação dos dados do autor.
8.	O utilizador confirma os dados do autor.
9.	Os passos 6 a 8 são repetidos até que na lista constem todos os autores do artigo.
10.	O sistema questiona qual dos autores introduzidos é o autor correspondente.
11.	O utilizador indica quem é o autor correspondente.
12.	O sistema solicita o ficheiro PDF correspondente ao conteúdo do artigo.
13.	O utilizador indica o respetivo ficheiro.
14.	O sistema valida e apresenta os dados introduzidos para confirmação.
15.	O utilizador confirma a informação apresentada.
16.	O sistema informa o utilizador do sucesso da operação.
###	Extensões (ou fluxos alternativos)
*a. A qualquer momento o utilizador solicita cancelamento da submissão. 
+ O caso de uso termina.

## SSD de formato completo
![UC4 completo.png](https://bitbucket.org/repo/aA9RGB/images/3938553861-UC4%20completo.png)

7a. Dados mínimos do autor obrigatórios em falta.

1. O sistema informa quais os dados em falta.
2. O sistema permite a introdução dos dados em falta (passo 6).
	
    2a. O utilizador não altera os dados. O caso de uso termina.

7b. O sistema detecta que os dados introduzidos correspondem potencialmente a um utilizador registado no sistema.

1. O sistema alerta para o facto e questiona se se trata da mesma pessoa.
2. O utilizador confirma que é a mesma pessoa.
3. O sistema faz a respetiva associação ao utilizador registado no sistema.

    3a. O utilizador rejeita a associação. O caso de uso prossegue normalmente (Passo 8).

14a. O sistema deteta dados mínimos obrigatório em falta.

1.	O sistema indica quais os dados em falta e solicita o seu preenchimento.
2.	O utilizador preenche os dados em falta (Passo 5).

###	Requisitos especiais
-
###	Tecnologia e Lista de Variações dos Dados
-
###	Frequência de Ocorrência
-
###	Questões em aberto
+	Quais são os dados necessários de cada autor?
+	Existe alguma limitação à extensão do resumo?
+	É possível a pessoa que está submeter não ser um dos autores do artigo?
+	Como é que o sistema sabe quais os eventos aos quais se pode submeter artigos?
+	Através de que informação pode o sistema detetar que um autor já está potencialmente registado como utilizador?
+	Podem ser submetidos outros formatos que não o PDF (e.g. Word)? 
+	Deve ser sugerido algum autor correspondente por omissão?
+	Qual o nível de detalhe exigido na identificação da instituição de afiliação (e.g., “nome da instituição”, “nome da instituição + país”, “nome da instituição +cidade+país”, “nome da instituição + morada completa (rua, nº de porta, código postal, cidade, país)”)?
