# UC2: Criar evento científico
##	Formato breve
O administrador introduz no sistema os dados do evento (título, descrição, data de início, data de fim, o local de realização). O sistema solicita os dados dos organizadores e o administrador introduz os dados de cada organizador. O sistema valida os dados introduzidos e pede confirmação. O sistema notifica que o registo do evento foi concluído com sucesso.

##	SSD de formato breve
![UC2.jpg](https://bitbucket.org/repo/aA9RGB/images/2174182883-UC2.jpg)

## Formato completo
### Ator principal
Administrador

### Partes interessadas e seus interesses
+ Administrador: rapidez no registo do evento.
+ Organizadores do evento: garantir acesso controlado aos seus eventos.

### Pré-condições
O utilizador está identificado e autenticado no sistema como administrador, i.e. só o administrador pode realizar esta atividade.

### Pós-condições
+ O evento fica armazenado no sistema.
+ Implica a classificação de utilizadores como “Organizadores”.

### Cenário de sucesso principal (ou fluxo básico)
1. O administrador inicia no sistema o registo de um evento.
2. O sistema solicita os dados do evento (título, descrição, data de início, data de fim, o local de realização).
3. O administrador introduz os dados solicitados.
4. O sistema solicita a introdução dos organizadores do evento.
5. O administrador introduz a identificação de um organizador.
6. O sistema valida e guarda os dados do organizador.
7. Os passos 5 a 6 repetem-se até que todos os organizadores do evento estejam inseridos.
8. O sistema valida e pede confirmação.
9. O administrador confirma a informação apresentada.
10. O sistema regista o evento e informa o administrador do sucesso da operação.

###Extensões (ou fluxos alternativos)
*a. A qualquer momento o administrador solicita cancelamento da criação de evento. 

+ O caso de uso termina.

6a. A identificação introduzida não corresponde a nenhum utilizador do sistema.

1. O sistema alerta para o facto.
2. O sistema permite a alteração da identificação introduzida (Passo 5).	

6b. O sistema deteta que o organizador indicado já foi incluído anteriormente.

1. O sistema alerta para o facto.
2. O sistema permite a alteração da identificação introduzida (Passo 4).

8a. Dados mínimos obrigatórios em falta.

1. O sistema informa quais os dados em falta.
2. O sistema permite a introdução dos dados em falta (Passo 3)
    2a. O administrador não altera os dados. O caso de uso termina.

8b. O sistema deteta que os dados (ou algum subconjunto de dados) introduzidos devem ser únicos e que já existem no sistema.

1. O sistema alerta o administrador para o facto (e mostra o evento que tem os mesmos dados).
2. O sistema permite a sua alteração.
    2a. O administrador não altera os dados. O caso de uso termina.

8c. O sistema deteta que algum dos dados não é válido.

1.	O sistema alerta o administrador para o facto (e mostra quais os dados inválidos)
2.	O sistema permite a sua alteração (Passo 3)
    2a. O administrador não altera os dados. O caso de uso termina.

### Requisitos especiais
-

### Tecnologia e Lista de Variações dos Dados
-

### Frequência de Ocorrência
- 

### Questões em aberto
+ Deve ser considerado algum fuso horário específico para as datas?
+ Deve haver algum atributo “tipo de evento” necessário para a criação do evento, ou essa informação estará (ou poderá estar) apenas no título do evento (e.g., “Conferência Internacional de Engenharia de Software 2014”)?
+ Qual a documentação requerida para o registo? Se alguma é exigida, a mesma deve ficar associada ao registo do evento no sistema?
+ Quais os dados exigidos para os organizadores do evento?
+ O que fazer quando um dos organizadores não se encontrar registado no sistema como utilizador aquando da criação de evento?
+ Quais são os dados obrigatórios para a criação do evento?
+ Quais os dados que em conjunto permitem detectar a duplicação de eventos?
+ Qual o nível de detalhe exigido na identificação do local do evento (e.g., “país”, “cidade+país”, “nome da instituição que acolhe o evento (universidade, hotel) + cidade + país”, “nome da instituição que acolhe o evento (universidade, hotel) + morada completa (rua, nº de porta, código postal, cidade, país)”)?
+ Alguma pessoa da instituição deve ser notificada da criação do evento no sistema como, por exemplo, o seu presidente?
+ Qual a frequência deste UC?