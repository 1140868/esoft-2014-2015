# Glossário
+ **Administrador do sistema**: Pessoa responsável pela tarefas administrativas do sistema, como por exemplo a criação de eventos científicos.

+ **Artigo (científico)**: Documento que descreve um trabalho científico desenvolvido por uma ou várias pessoas. No contexto deste documento, “artigo” e “artigo científico” têm o mesmo significado.

+ **Autor**: Pessoa que contribui para a escrita de uma artigo e/ou o trabalho descrito no mesmo. 

+ **Autor correspondente**: É um dos autores do artigo que recebe as notificações relativas a uma submissão.

+ **Comissão de Programa**: Conjunto de pessoas com determinadas responsabilidades comuns. Tipicamente ajudam na divulgação do evento e fazem a revisão de artigos para um evento.

+ **CP**: Abreviatura de “Comissão de Programa”.

+ **Credenciais de Acesso**: Informação que permite autenticar univocamente um utilizador perante o sistema. Numa das suas formas mais simples corresponde a um par username/password.

+ **Empresa**: Entidade que adquiriu a aplicação. AKA dono do software.

+ **Evento Científico**: Evento no qual vários trabalhos científicos são apresentados , desenvolvidos e/ou ensinados, dependendo do seu âmbito. Muitos desses eventos exigem a apresentação e a revisão prévia de artigos antes da sua data de início.

+ **Instituição de afiliação**: Instituição a qual uma pessoa está associada, muitas vezes através de uma relação de trabalho e/ou investigação que pode ter diferentes níveis de formalidade.

+ **Local de realização**: TBD

+ **Membro da CP**: Ver “revisor”.

+ **Organizador**: Pessoa que é responsável pela realização de um evento, juntamente ou não com outras pessoas.

+ **PDF**: Abreviatura do inglês para Portable Document Format.

+ **Período (do evento)**: Corresponde às datas de início e de fim do evento.

+ **Registo de utilizador**: Processo através do qual uma pessoa solicita o acesso ao sistema por forma a operar sobre o sistema (e.g. autor).

+ **Resumo de artigo**: Texto sucinto e objectivo que sumaria o conteúdo do artigo. Tipicamente tem um limite mínimo e máximo de caracteres.

+ **Revisão (de Artigo)**: Processo conduzido por uma ou mais membros da Comissão de Programa (Revisores) no qual se analisa/avalia o interesse/relevância de um artigo no âmbito de um evento científico.

+ **Revisor**: Membro da CP, mas o termo enfatiza a função de rever artigos submetidos de forma a assegurar a qualidade dos artigos aceites no evento.

+ **Submissão (de Artigo)**: Processo através do qual um dos autores de um artigo científico manifesta interesse em que esse artigo venha a ser divulgado num evento. 

+ **Utilizador**: Representa uma pessoa que utiliza o sistema.

+ **Utilizador não registado**: Representa uma pessoa que utiliza o sistema anonimamente e se pode registar.




