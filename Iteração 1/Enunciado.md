# Enunciado da Iteração 1

A empresa pretende desenvolver uma aplicação (de software) de apoio à organização e gestão de eventos científicos. Entre outras coisas, este software deve permitir registar vários eventos e os seus organizadores, bem como facilitar o processo de submissão de artigos científicos aos respetivos eventos.

Um evento científico tem um título, um texto descritivo sobre o âmbito do mesmo, o período e o local de realização, e um conjunto de pessoas responsáveis pela sua realização (os organizadores). A criação dos eventos científicos é feita por um administrador do sistema. Numa fase posterior à criação de um evento no sistema, um dos organizadores define quais os membros da Comissão de Programa (CP), que são as pessoas que farão posteriormente a revisão dos artigos (os revisores).

Aquando da submissão de um artigo científico (tipicamente um documento em formato PDF) o autor deve especificar quais são os restantes autores do mesmo, o título do artigo e um resumo, e identificar quem é o autor correspondente.  Os dados de um autor são o seu nome e instituição de afiliação.

É necessário que todos os organizadores, revisores e quem submete artigos científicos estejam registados no sistema como utilizadores. Um utilizador tem um nome, um email e credenciais de acesso, armazenadas de forma segura.

Com o intuito de potenciar a interoperabilidade com outros sistema já existentes e/ou em desenvolvimento, o núcleo principal do software deve ser desenvolvido em Java.